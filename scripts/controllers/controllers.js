app.controller('GalleryController', ['$scope', '$http', 'Lightbox', function ( $scope, $http, Lightbox ) {
 
  $http.get('data/images.json').success(function(data) {
    $scope.images = data;
  });

  $scope.openLightboxModal = function (index) {
    Lightbox.openModal($scope.images, index);
  };
  
}]);
