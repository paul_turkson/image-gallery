var app = angular.module('galleryApp', ['ngRoute', 'bootstrapLightbox']);

app.config(['$routeProvider', function ($routeProvider) {
	$routeProvider
		.when('/', {templateUrl: 'partials/gallery.html', controller: 'GalleryController'})
		.otherwise({redirectTo: '/'});
}]);


// The original lightbox was a bit small so I've put in the code from the documentation that allows it to be bigger.
app.config(function (LightboxProvider) {
  // set a custom template
  LightboxProvider.templateUrl = 'partials/lightbox.html';

  LightboxProvider.getImageUrl = function (image) {
    return image.src;
  };

  LightboxProvider.getImageCaption = function (image) {
    return image.caption;
  };

  // increase the maximum display height of the image
  LightboxProvider.calculateImageDimensionLimits = function (dimensions) {
    return {
      'maxWidth': dimensions.windowWidth >= 768 ? // default
        dimensions.windowWidth - 92 :
        dimensions.windowWidth - 52,
      'maxHeight': 1600                           // custom
    };
  };

  // the modal height calculation has to be changed since our custom template is
  // taller than the default template
  LightboxProvider.calculateModalDimensions = function (dimensions) {
    var width = Math.max(400, dimensions.imageDisplayWidth + 32);

    if (width >= dimensions.windowWidth - 20 || dimensions.windowWidth < 768) {
      width = 'auto';
    }

    return {
      'width': width,                             // default
      'height': 'auto'                            // custom
    };
  };
  
});