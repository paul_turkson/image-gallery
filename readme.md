On a Mac or Linux, from the terminal run python -m SimpleHTTPServer in the code directory and navigate to the address. 
It should be either be http://127.0.0.1:8000, http://192.168.1.2:8000 or localhost:8000

On Windows (which this was developed on), I used Xampp so I can use localhost to server the files. The python command can also be run if python is install on the machine.

The url to navigate to should be similar to this: localhost:8000/#/ -> take note of the #/ as this is the location of app

The code all lives within the app directory and is split out into a project structure of data, partials, scripts, sass. styles, and index.html

All dependencies are declared within bower.json and they can be found in bower_components.

The file that is generating the images images.json that is found within the data folder.

Sass was used to make it easier to maintain the main page and lightbox stylings.

I used Angular Bootstrap Lightbox instead of making the lightbox code from scratch - the was due to timing as I was unsure whether design time was included in the 2 hours.

The project is reponsive and has 3 breakpoints - 2 columns, then 3 and then 6 columns. 